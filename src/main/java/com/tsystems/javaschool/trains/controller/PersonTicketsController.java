package com.tsystems.javaschool.trains.controller;

import com.tsystems.javaschool.trains.dto.PersonTicketsInfo;
import com.tsystems.javaschool.trains.service.PersonTicketsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/personTickets")
public class PersonTicketsController {
    private final PersonTicketsService personTicketsService;

    public PersonTicketsController(PersonTicketsService personTicketsService) {
        this.personTicketsService = personTicketsService;
    }

    @GetMapping("/")
    public List<PersonTicketsInfo> getAllTickets() {
        return personTicketsService.getAllPersonTickets();
    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonTicketsInfo> getTicket(@PathVariable int id) {
        PersonTicketsInfo sto = personTicketsService.getPersonTickets(id);
        if (sto == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(sto);
    }
}