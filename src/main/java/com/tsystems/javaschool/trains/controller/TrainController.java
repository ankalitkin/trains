package com.tsystems.javaschool.trains.controller;

import com.tsystems.javaschool.trains.dao.TrainDao;
import com.tsystems.javaschool.trains.dto.TrainDto;
import com.tsystems.javaschool.trains.mappers.TrainMapper;
import com.tsystems.javaschool.trains.model.Train;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/train")
public class TrainController {
    private final TrainDao trainDao;
    private final TrainMapper trainMapper;

    public TrainController(TrainDao trainDao, TrainMapper trainMapper) {
        this.trainDao = trainDao;
        this.trainMapper = trainMapper;
    }

    @GetMapping("/")
    public List<TrainDto> getAllTrains() {
        return trainMapper.toDto(trainDao.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<TrainDto> getTrain(@PathVariable int id) {
        Train train = trainDao.get(id);
        if (train == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(trainMapper.toDto(train));
    }

    @PutMapping("/{id}")
    public ResponseEntity<TrainDto> putTrain(@PathVariable int id, @RequestBody TrainDto trainDto) {
        if (trainDto.getId() != id)
            return ResponseEntity.badRequest().build();
        Train train = trainDao.get(id);
        if (train == null)
            return ResponseEntity.notFound().build();
        trainDao.update(trainMapper.toTrain(trainDto));
        return ResponseEntity.ok(trainDto);
    }

    @PostMapping("/")
    public ResponseEntity<TrainDto> postTrain(@RequestBody TrainDto trainDto) {
        int id = trainDao.add(trainMapper.toTrain(trainDto));
        TrainDto newDto = trainMapper.toDto(trainDao.get(id));
        return ResponseEntity.ok(newDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<TrainDto> deleteTrain(@PathVariable int id) {
        Train train = trainDao.get(id);
        if (train == null)
            return ResponseEntity.notFound().build();
        trainDao.delete(train);
        return ResponseEntity.ok(trainMapper.toDto(train));
    }
}