package com.tsystems.javaschool.trains.controller;

import com.tsystems.javaschool.trains.dto.TrainTimetableInfo;
import com.tsystems.javaschool.trains.service.TrainTimetableService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/trainTimetable")
public class TrainTimetableController {
    private final TrainTimetableService trainTimetableService;

    public TrainTimetableController(TrainTimetableService trainTimetableService) {
        this.trainTimetableService = trainTimetableService;
    }

    @GetMapping("/")
    public List<TrainTimetableInfo> getAllTimetables() {
        return trainTimetableService.getAllTrainTimetables();
    }

    @GetMapping("/{id}")
    public ResponseEntity<TrainTimetableInfo> getTimetable(@PathVariable int id) {
        TrainTimetableInfo sto = trainTimetableService.getTrainTimetable(id);
        if (sto == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(sto);
    }
}