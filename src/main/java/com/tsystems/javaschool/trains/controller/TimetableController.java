package com.tsystems.javaschool.trains.controller;

import com.tsystems.javaschool.trains.dao.TimetableDao;
import com.tsystems.javaschool.trains.dto.TimetableDto;
import com.tsystems.javaschool.trains.mappers.TimetableMapper;
import com.tsystems.javaschool.trains.model.Timetable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/timetable")
public class TimetableController {
    private final TimetableDao timetableDao;
    private final TimetableMapper timetableMapper;

    public TimetableController(TimetableDao timetableDao, TimetableMapper timetableMapper) {
        this.timetableDao = timetableDao;
        this.timetableMapper = timetableMapper;
    }

    @GetMapping("/")
    public List<TimetableDto> getAllTimetables() {
        return timetableMapper.toDto(timetableDao.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<TimetableDto> getTimetable(@PathVariable int id) {
        Timetable timetable = timetableDao.get(id);
        if (timetable == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(timetableMapper.toDto(timetable));
    }

    @PutMapping("/{id}")
    public ResponseEntity<TimetableDto> putTimetable(@PathVariable int id, @RequestBody TimetableDto timetableDto) {
        if (timetableDto.getId() != id)
            return ResponseEntity.badRequest().build();
        Timetable timetable = timetableDao.get(id);
        if (timetable == null)
            return ResponseEntity.notFound().build();
        timetableDao.update(timetableMapper.toTimetable(timetableDto));
        return ResponseEntity.ok(timetableDto);
    }

    @PostMapping("/")
    public ResponseEntity<TimetableDto> postTimetable(@RequestBody TimetableDto timetableDto) {
        int id = timetableDao.add(timetableMapper.toTimetable(timetableDto));
        TimetableDto newDto = timetableMapper.toDto(timetableDao.get(id));
        return ResponseEntity.ok(newDto);
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<TimetableDto> deleteTimetable(@PathVariable int id) {
        Timetable timetable = timetableDao.get(id);
        if (timetable == null)
            return ResponseEntity.notFound().build();
        timetableDao.delete(timetable);
        return ResponseEntity.ok(timetableMapper.toDto(timetable));
    }
}