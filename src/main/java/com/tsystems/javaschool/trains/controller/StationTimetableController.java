package com.tsystems.javaschool.trains.controller;

import com.tsystems.javaschool.trains.dto.StationTimetableInfo;
import com.tsystems.javaschool.trains.service.StationTimetableService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/stationTimetable")
public class StationTimetableController {
    private final StationTimetableService stationTimetableService;

    public StationTimetableController(StationTimetableService stationTimetableService) {
        this.stationTimetableService = stationTimetableService;
    }

    @GetMapping("/")
    public List<StationTimetableInfo> getAllTimetables() {
        return stationTimetableService.getAllStationTimetables();
    }

    @GetMapping("/{id}")
    public ResponseEntity<StationTimetableInfo> getTimetable(@PathVariable int id) {
        StationTimetableInfo sto = stationTimetableService.getStationTimetable(id);
        if (sto == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(sto);
    }
}