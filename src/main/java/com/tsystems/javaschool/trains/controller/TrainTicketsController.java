package com.tsystems.javaschool.trains.controller;

import com.tsystems.javaschool.trains.dto.TrainTicketsInfo;
import com.tsystems.javaschool.trains.service.TrainTicketsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/trainTickets")
public class TrainTicketsController {
    private final TrainTicketsService trainTicketsService;

    public TrainTicketsController(TrainTicketsService trainTicketsService) {
        this.trainTicketsService = trainTicketsService;
    }

    @GetMapping("/")
    public List<TrainTicketsInfo> getAllTickets() {
        return trainTicketsService.getAllTrainTickets();
    }

    @GetMapping("/{id}")
    public ResponseEntity<TrainTicketsInfo> getTicket(@PathVariable int id) {
        TrainTicketsInfo sto = trainTicketsService.getTrainTickets(id);
        if (sto == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(sto);
    }
}