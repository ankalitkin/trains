package com.tsystems.javaschool.trains.controller;

import com.tsystems.javaschool.trains.dto.SearchQuery;
import com.tsystems.javaschool.trains.dto.SearchResultInfo;
import com.tsystems.javaschool.trains.mappers.SearchResultMapper;
import com.tsystems.javaschool.trains.service.TrainSearchService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/search")
public class TrainSearchController {
    private final TrainSearchService trainSearchService;
    private final SearchResultMapper searchResultMapper;

    public TrainSearchController(TrainSearchService trainSearchService, SearchResultMapper searchResultMapper) {
        this.trainSearchService = trainSearchService;
        this.searchResultMapper = searchResultMapper;
    }

    @GetMapping("/{from}/{to}")
    public List<SearchResultInfo> searchTrains(@PathVariable("from") int from, @PathVariable("to") int to) {
        return searchResultMapper.toInfo(trainSearchService.get(from, to));
    }

    @GetMapping("/")
    public List<SearchResultInfo> searchTrains(@RequestBody SearchQuery searchQuery) {
        int from = searchQuery.getFromStationId();
        int to = searchQuery.getToStationId();
        String minTime = searchQuery.getMinTime();
        String maxTime = searchQuery.getMaxTime();
        return searchResultMapper.toInfo(trainSearchService.get(from, to, minTime, maxTime));
    }
}