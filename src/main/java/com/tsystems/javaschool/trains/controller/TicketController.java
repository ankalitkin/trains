package com.tsystems.javaschool.trains.controller;

import com.tsystems.javaschool.trains.dao.TicketDao;
import com.tsystems.javaschool.trains.dto.TicketInfo;
import com.tsystems.javaschool.trains.mappers.TicketMapper;
import com.tsystems.javaschool.trains.model.Ticket;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/ticket")
public class TicketController {
    private final TicketDao ticketDao;
    private final TicketMapper ticketMapper;

    public TicketController(TicketDao ticketDao, TicketMapper ticketMapper) {
        this.ticketDao = ticketDao;
        this.ticketMapper = ticketMapper;
    }

    @GetMapping("/")
    public List<TicketInfo> getAllTickets() {
        return ticketMapper.toInfo(ticketDao.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<TicketInfo> getTicket(@PathVariable int id) {
        Ticket ticket = ticketDao.get(id);
        if (ticket == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(ticketMapper.toInfo(ticket));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<TicketInfo> deleteTicket(@PathVariable int id) {
        Ticket ticket = ticketDao.get(id);
        if (ticket == null)
            return ResponseEntity.notFound().build();
        ticketDao.delete(ticket);
        return ResponseEntity.ok(ticketMapper.toInfo(ticket));
    }
}