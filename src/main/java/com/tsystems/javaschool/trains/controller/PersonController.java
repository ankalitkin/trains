package com.tsystems.javaschool.trains.controller;

import com.tsystems.javaschool.trains.dao.PersonDao;
import com.tsystems.javaschool.trains.dto.PersonDto;
import com.tsystems.javaschool.trains.mappers.PersonMapper;
import com.tsystems.javaschool.trains.model.Person;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/person")
public class PersonController {
    private final PersonDao personDao;
    private final PersonMapper personMapper;

    public PersonController(PersonDao personDao, PersonMapper personMapper) {
        this.personDao = personDao;
        this.personMapper = personMapper;
    }

    @GetMapping("/")
    public List<PersonDto> getAllPersons() {
        return personMapper.toDto(personDao.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonDto> getPerson(@PathVariable int id) {
        Person person = personDao.get(id);
        if (person == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(personMapper.toDto(person));
    }

    @GetMapping("/find")
    public ResponseEntity<PersonDto> findPerson(@RequestBody PersonDto personDto) {
        Person person = personDao.find(personDto.getName(), personDto.getSurname(), personDto.getBirthday());
        if (person == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(personMapper.toDto(person));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PersonDto> putPerson(@PathVariable int id, @RequestBody PersonDto personDto) {
        if (personDto.getId() != id)
            return ResponseEntity.badRequest().build();
        Person person = personDao.get(id);
        if (person == null)
            return ResponseEntity.notFound().build();
        personDao.update(personMapper.toPerson(personDto));
        return ResponseEntity.ok(personDto);
    }

    @PostMapping("/")
    public ResponseEntity<PersonDto> postPerson(@RequestBody PersonDto personDto) {
        int id = personDao.add(personMapper.toPerson(personDto));
        PersonDto newDto = personMapper.toDto(personDao.get(id));
        return ResponseEntity.ok(newDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<PersonDto> deletePerson(@PathVariable int id) {
        Person person = personDao.get(id);
        if (person == null)
            return ResponseEntity.notFound().build();
        personDao.delete(person);
        return ResponseEntity.ok(personMapper.toDto(person));
    }
}