package com.tsystems.javaschool.trains.controller;

import com.tsystems.javaschool.trains.dao.StationDao;
import com.tsystems.javaschool.trains.dto.StationDto;
import com.tsystems.javaschool.trains.mappers.StationMapper;
import com.tsystems.javaschool.trains.model.Station;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/station")
public class StationController {
    private final StationDao stationDao;
    private final StationMapper stationMapper;

    public StationController(StationDao stationDao, StationMapper stationMapper) {
        this.stationDao = stationDao;
        this.stationMapper = stationMapper;
    }

    @GetMapping("/")
    public List<StationDto> getAllStations() {
        return stationMapper.toDto(stationDao.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<StationDto> getStation(@PathVariable int id) {
        Station station = stationDao.get(id);
        if (station == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(stationMapper.toDto(station));
    }

    @PutMapping("/{id}")
    public ResponseEntity<StationDto> putStation(@PathVariable int id, @RequestBody StationDto stationDto) {
        if (stationDto.getId() != id)
            return ResponseEntity.badRequest().build();
        Station station = stationDao.get(id);
        if (station == null)
            return ResponseEntity.notFound().build();
        stationDao.update(stationMapper.toStation(stationDto));
        return ResponseEntity.ok(stationDto);
    }

    @PostMapping("/")
    public ResponseEntity<StationDto> postStation(@RequestBody StationDto stationDto) {
        int id = stationDao.add(stationMapper.toStation(stationDto));
        StationDto newDto = stationMapper.toDto(stationDao.get(id));
        return ResponseEntity.ok(newDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<StationDto> deleteStation(@PathVariable int id) {
        Station station = stationDao.get(id);
        if (station == null)
            return ResponseEntity.notFound().build();
        stationDao.delete(station);
        return ResponseEntity.ok(stationMapper.toDto(station));
    }
}