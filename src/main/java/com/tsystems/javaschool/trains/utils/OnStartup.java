package com.tsystems.javaschool.trains.utils;

import com.tsystems.javaschool.trains.dao.*;
import com.tsystems.javaschool.trains.model.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class OnStartup implements ApplicationListener<ContextRefreshedEvent> {
    private final PersonDao personDao;
    private final TrainDao trainDao;
    private final StationDao stationDao;
    private final TicketDao ticketDao;
    private final TimetableDao timetableDao;

    public OnStartup(PersonDao personDao, TrainDao trainDao, StationDao stationDao,
                     TicketDao ticketDao, TimetableDao timetableDao) {
        this.personDao = personDao;
        this.trainDao = trainDao;
        this.stationDao = stationDao;
        this.ticketDao = ticketDao;
        this.timetableDao = timetableDao;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (personDao.getAll().isEmpty()) {
            Person[] persons = new Person[]{
                    new Person("Вася", "Пупкин", "2000-01-01"),
                    new Person("Петя", "Васечкин", "1999-08-19")};
            personDao.addRange(persons);

            Train electron = new Train("Воронеж - Лиски", 10);
            trainDao.add(electron);

            Station[] stations = new Station[]{
                    new Station("Воронеж"),
                    new Station("Берёзовая роща"),
                    new Station("Отрожка"),
                    new Station("Придача"),
                    new Station("Машмет"),
                    new Station("Масловка"),
                    new Station("Боево"),
                    new Station("Колодезная"),
                    new Station("Аношкино"),
                    new Station("Давыдовка"),
                    new Station("Бодеево"),
                    new Station("Блочный завод"),
                    new Station("Депо"),
                    new Station("Лиски")
            };
            stationDao.addRange(stations);

            Timetable[] timetable = new Timetable[]{
                    new Timetable(stations[0], electron, "15:10:00", "15:11:00"),
                    new Timetable(stations[1], electron, "15:17:00", "15:18:00"),
                    new Timetable(stations[2], electron, "15:25:00", "15:26:00"),
                    new Timetable(stations[3], electron, "15:40:00", "15:41:00"),
                    new Timetable(stations[4], electron, "15:47:00", "15:48:00"),
                    new Timetable(stations[6], electron, "16:05:00", "16:06:00"),
                    new Timetable(stations[7], electron, "16:17:00", "16:18:00"),
                    new Timetable(stations[8], electron, "16:27:00", "16:28:00"),
                    new Timetable(stations[9], electron, "16:36:00", "16:37:00"),
                    new Timetable(stations[10], electron, "16:48:00", "16:49:00"),
                    new Timetable(stations[11], electron, "16:53:00", "16:54:00"),
                    new Timetable(stations[13], electron, "17:02:00", "17:03:00"),
            };
            timetableDao.addRange(timetable);

            Ticket[] tickets = new Ticket[]{
                    new Ticket(persons[0], electron),
                    new Ticket(persons[1], electron)
            };
            ticketDao.addRange(tickets);
        }
    }
}