package com.tsystems.javaschool.trains.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "train")
public class Train {
    @Id
    @GeneratedValue
    private Integer id;

    @NotEmpty(message = "{train.name.empty}")
    private String name;

    @Min(1)
    private Integer numberOfSeats;

    @Setter(AccessLevel.NONE)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "train")
    @JsonIgnore
    private List<Ticket> tickets;

    @Setter(AccessLevel.NONE)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "train")
    private List<Timetable> timetable;

    public Train(String name, Integer numberOfSeats) {
        this.name = name;
        this.numberOfSeats = numberOfSeats;
    }
}
