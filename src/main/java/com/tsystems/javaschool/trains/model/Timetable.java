package com.tsystems.javaschool.trains.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table(name = "timetable")
public class Timetable {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "station_id")
    private Station station;

    @ManyToOne
    @JoinColumn(name = "train_id")
    private Train train;

    @Column(name = "arrival")
    private String arrival;

    @Column(name = "departure")
    private String departure;

    public Timetable(Station station, Train train, String arrival, String departure) {
        this.station = station;
        this.train = train;
        this.arrival = arrival;
        this.departure = departure;
    }

    public Timetable(Station station, Train train, String time) {
        this(station, train, time, time);
    }

}
