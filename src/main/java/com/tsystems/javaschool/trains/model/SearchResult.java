package com.tsystems.javaschool.trains.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SearchResult {
    private Train train;
    private Timetable departure;
    private Timetable arrival;
}
