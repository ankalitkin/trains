package com.tsystems.javaschool.trains.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "person", uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "surname", "birthday"})})
public class Person {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @NotEmpty(message = "{user.name.empty}")
    @Column(name = "name")
    private String name;

    @NotEmpty(message = "{user.surname.empty}")
    @Column(name = "surname")
    private String surname;

    @Column(name = "birthday")
    private String birthday;

    @Setter(AccessLevel.NONE)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "person")
    private List<Ticket> tickets;

    public Person(String name, String surname, String birthday) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }
}

