package com.tsystems.javaschool.trains.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "station")
public class Station {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @NotEmpty(message = "{station.name.empty}")
    @Column(name = "name")
    private String name;

    @Setter(AccessLevel.NONE)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "station")
    private List<Timetable> timetable;

    public Station(String name) {
        this.name = name;
    }
}