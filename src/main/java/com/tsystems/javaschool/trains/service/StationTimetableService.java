package com.tsystems.javaschool.trains.service;

import com.tsystems.javaschool.trains.dao.StationDao;
import com.tsystems.javaschool.trains.dto.StationTimetableInfo;
import com.tsystems.javaschool.trains.dto.TimetableDto;
import com.tsystems.javaschool.trains.mappers.TimetableMapper;
import com.tsystems.javaschool.trains.model.Station;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class StationTimetableService {
    private final StationDao stationDao;
    private final TimetableMapper timetableMapper;

    public StationTimetableService(StationDao stationDao, TimetableMapper timetableMapper) {
        this.stationDao = stationDao;
        this.timetableMapper = timetableMapper;
    }

    public List<StationTimetableInfo> getAllStationTimetables() {
        List<Station> stations = stationDao.getAll();
        return stations.stream().map(this::getStationTimetable).collect(Collectors.toList());
    }

    public StationTimetableInfo getStationTimetable(int id) {
        Station station = stationDao.get(id);
        if (station == null)
            return null;
        return getStationTimetable(station);
    }

    private StationTimetableInfo getStationTimetable(Station station) {
        StationTimetableInfo stationTimetableInfo = new StationTimetableInfo();
        stationTimetableInfo.setId(station.getId());
        stationTimetableInfo.setName(station.getName());
        List<TimetableDto> timetable = station.getTimetable().stream().map(timetableMapper::toDto).collect(Collectors.toList());
        stationTimetableInfo.setTimetable(timetable);
        return stationTimetableInfo;
    }
}
