package com.tsystems.javaschool.trains.service;

import com.tsystems.javaschool.trains.dao.TrainDao;
import com.tsystems.javaschool.trains.dto.TimetableInfo;
import com.tsystems.javaschool.trains.dto.TrainTimetableInfo;
import com.tsystems.javaschool.trains.mappers.TimetableMapper;
import com.tsystems.javaschool.trains.model.Train;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class TrainTimetableService {
    private final TrainDao trainDao;
    private final TimetableMapper timetableMapper;

    public TrainTimetableService(TrainDao trainDao, TimetableMapper timetableMapper) {
        this.trainDao = trainDao;
        this.timetableMapper = timetableMapper;
    }

    public List<TrainTimetableInfo> getAllTrainTimetables() {
        List<Train> trains = trainDao.getAll();
        return trains.stream().map(this::getTrainTimetable).collect(Collectors.toList());
    }

    public TrainTimetableInfo getTrainTimetable(int id) {
        Train train = trainDao.get(id);
        if (train == null)
            return null;
        return getTrainTimetable(train);
    }

    private TrainTimetableInfo getTrainTimetable(Train train) {
        TrainTimetableInfo trainTimetableInfo = new TrainTimetableInfo();
        trainTimetableInfo.setId(train.getId());
        trainTimetableInfo.setName(train.getName());
        trainTimetableInfo.setNumberOfSeats(train.getNumberOfSeats());
        List<TimetableInfo> timetable = train.getTimetable().stream().map(timetableMapper::toInfo).collect(Collectors.toList());
        trainTimetableInfo.setTimetable(timetable);
        return trainTimetableInfo;
    }
}
