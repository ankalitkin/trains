package com.tsystems.javaschool.trains.service;

import com.tsystems.javaschool.trains.model.SearchResult;
import com.tsystems.javaschool.trains.model.Timetable;
import com.tsystems.javaschool.trains.model.Train;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class TrainSearchService {

    private final SessionFactory sessionFactory;

    public TrainSearchService(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<SearchResult> get(int departureStationId, int arrivalStationId) {
        return get(departureStationId, arrivalStationId, null, null);
    }

    public List<SearchResult> get(int departureStationId, int arrivalStationId, String minTime, String maxTime) {
        minTime = minTime != null ? minTime : "00:00:00";
        maxTime = maxTime != null ? maxTime : "23:59:59";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(
                "Select distinct tr, dep, arv from Train tr " +
                        "join Timetable dep on tr = dep.train " +
                        "join Timetable arv on tr = arv.train " +
                        "where dep.station.id = :depStationId " +
                        "and arv.station.id = :arvStationId " +
                        "and dep.departure <= arv.arrival " +
                        "and :minTime <= dep.departure " +
                        "and dep.departure <= :maxTime")
                .setParameter("depStationId", departureStationId)
                .setParameter("arvStationId", arrivalStationId)
                .setParameter("minTime", minTime)
                .setParameter("maxTime", maxTime);
        List<SearchResult> searchResults = (List<SearchResult>) query.list().stream().map(object -> {
            Object[] arr = (Object[]) object;
            Train train = (Train) arr[0];
            Timetable departure = (Timetable) arr[1];
            Timetable arrival = (Timetable) arr[2];
            return new SearchResult(train, departure, arrival);
        }).collect(Collectors.toList());
        return searchResults;
    }

}
