package com.tsystems.javaschool.trains.service;

import com.tsystems.javaschool.trains.dao.PersonDao;
import com.tsystems.javaschool.trains.dto.PersonTicketsInfo;
import com.tsystems.javaschool.trains.dto.TicketInfo;
import com.tsystems.javaschool.trains.mappers.TicketMapper;
import com.tsystems.javaschool.trains.model.Person;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class PersonTicketsService {
    private final PersonDao personDao;
    private final TicketMapper ticketsMapper;

    public PersonTicketsService(PersonDao personDao, TicketMapper ticketsMapper) {
        this.personDao = personDao;
        this.ticketsMapper = ticketsMapper;
    }

    public List<PersonTicketsInfo> getAllPersonTickets() {
        List<Person> persons = personDao.getAll();
        return persons.stream().map(this::getPersonTickets).collect(Collectors.toList());
    }

    public PersonTicketsInfo getPersonTickets(int id) {
        Person person = personDao.get(id);
        if (person == null)
            return null;
        return getPersonTickets(person);
    }

    private PersonTicketsInfo getPersonTickets(Person person) {
        PersonTicketsInfo personTicketsInfo = new PersonTicketsInfo();
        personTicketsInfo.setId(person.getId());
        personTicketsInfo.setName(person.getName());
        personTicketsInfo.setSurname(person.getSurname());
        personTicketsInfo.setBirthday(person.getBirthday());
        List<TicketInfo> tickets = person.getTickets().stream().map(ticketsMapper::toInfo).collect(Collectors.toList());
        personTicketsInfo.setTickets(tickets);
        return personTicketsInfo;
    }
}
