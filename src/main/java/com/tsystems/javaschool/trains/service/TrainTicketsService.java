package com.tsystems.javaschool.trains.service;

import com.tsystems.javaschool.trains.dao.TrainDao;
import com.tsystems.javaschool.trains.dto.TicketInfo;
import com.tsystems.javaschool.trains.dto.TrainTicketsInfo;
import com.tsystems.javaschool.trains.mappers.TicketMapper;
import com.tsystems.javaschool.trains.model.Train;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class TrainTicketsService {
    private final TrainDao trainDao;
    private final TicketMapper ticketsMapper;

    public TrainTicketsService(TrainDao trainDao, TicketMapper ticketsMapper) {
        this.trainDao = trainDao;
        this.ticketsMapper = ticketsMapper;
    }

    public List<TrainTicketsInfo> getAllTrainTickets() {
        List<Train> trains = trainDao.getAll();
        return trains.stream().map(this::getTrainTickets).collect(Collectors.toList());
    }

    public TrainTicketsInfo getTrainTickets(int id) {
        Train train = trainDao.get(id);
        if (train == null)
            return null;
        return getTrainTickets(train);
    }

    private TrainTicketsInfo getTrainTickets(Train train) {
        TrainTicketsInfo trainTicketsInfo = new TrainTicketsInfo();
        trainTicketsInfo.setId(train.getId());
        trainTicketsInfo.setName(train.getName());
        trainTicketsInfo.setNumberOfSeats(train.getNumberOfSeats());
        List<TicketInfo> tickets = train.getTickets().stream().map(ticketsMapper::toInfo).collect(Collectors.toList());
        trainTicketsInfo.setTickets(tickets);
        return trainTicketsInfo;
    }
}
