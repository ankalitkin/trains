package com.tsystems.javaschool.trains.mappers;

import com.tsystems.javaschool.trains.dao.StationDao;
import com.tsystems.javaschool.trains.dao.TimetableDao;
import com.tsystems.javaschool.trains.dao.TrainDao;
import com.tsystems.javaschool.trains.dto.TimetableDto;
import com.tsystems.javaschool.trains.dto.TimetableInfo;
import com.tsystems.javaschool.trains.model.Timetable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TimetableMapper {
    private final StationMapper stationMapper;
    private final TimetableDao timetableDao;
    private final StationDao stationDao;
    private final TrainDao trainDao;

    public TimetableMapper(StationMapper stationMapper, TimetableDao timetableDao, StationDao stationDao, TrainDao trainDao) {
        this.stationMapper = stationMapper;
        this.timetableDao = timetableDao;
        this.stationDao = stationDao;
        this.trainDao = trainDao;
    }

    public TimetableDto toDto(Timetable timetable) {
        TimetableDto timetableDto = new TimetableDto();
        timetableDto.setId(timetable.getId());
        timetableDto.setArrival(timetable.getArrival());
        timetableDto.setDeparture(timetable.getDeparture());
        timetableDto.setTrain_id(timetable.getTrain().getId());
        timetableDto.setStation_id(timetable.getStation().getId());
        return timetableDto;
    }

    public TimetableInfo toInfo(Timetable timetable) {
        TimetableInfo timetableInfo = new TimetableInfo();
        timetableInfo.setId(timetable.getId());
        timetableInfo.setArrival(timetable.getArrival());
        timetableInfo.setDeparture(timetable.getDeparture());
        timetableInfo.setTrain_id(timetable.getTrain().getId());
        timetableInfo.setStation(stationMapper.toDto(timetable.getStation()));
        return timetableInfo;
    }

    public Timetable toTimetable(TimetableDto timetableDto) {
        Timetable timetable = timetableDao.get(timetableDto.getId());
        if (timetable != null)
            return timetable;
        timetable = new Timetable();
        timetable.setId(timetableDto.getId());
        timetable.setArrival(timetableDto.getArrival());
        timetable.setDeparture(timetableDto.getDeparture());
        timetable.setStation(stationDao.get(timetableDto.getStation_id()));
        timetable.setTrain(trainDao.get(timetableDto.getTrain_id()));
        return timetable;
    }

    public List<TimetableDto> toDto(List<Timetable> timetables) {
        return timetables.stream().map(this::toDto).collect(Collectors.toList());
    }
}