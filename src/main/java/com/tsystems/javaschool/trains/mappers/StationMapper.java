package com.tsystems.javaschool.trains.mappers;

import com.tsystems.javaschool.trains.dao.StationDao;
import com.tsystems.javaschool.trains.dto.StationDto;
import com.tsystems.javaschool.trains.model.Station;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StationMapper {
    private final StationDao stationDao;

    public StationMapper(StationDao stationDao) {
        this.stationDao = stationDao;
    }

    public StationDto toDto(Station station) {
        StationDto stationDto = new StationDto();
        stationDto.setId(station.getId());
        stationDto.setName(station.getName());
        return stationDto;
    }

    public Station toStation(StationDto stationDto) {
        Station station = stationDao.get(stationDto.getId());
        if (station != null)
            return station;
        station = new Station();
        station.setId(stationDto.getId());
        station.setName(stationDto.getName());
        return station;
    }

    public List<StationDto> toDto(List<Station> stations) {
        return stations.stream().map(this::toDto).collect(Collectors.toList());
    }
}