package com.tsystems.javaschool.trains.mappers;

import com.tsystems.javaschool.trains.dao.PersonDao;
import com.tsystems.javaschool.trains.dto.PersonDto;
import com.tsystems.javaschool.trains.model.Person;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PersonMapper {
    private final PersonDao personDao;

    public PersonMapper(PersonDao personDao) {
        this.personDao = personDao;
    }

    public PersonDto toDto(Person person) {
        PersonDto personDto = new PersonDto();
        personDto.setId(person.getId());
        personDto.setName(person.getName());
        personDto.setSurname(person.getSurname());
        personDto.setBirthday(person.getBirthday());
        return personDto;
    }

    public Person toPerson(PersonDto personDto) {
        Person person = personDao.find(personDto.getName(), personDto.getSurname(), personDto.getBirthday());
        if (person != null)
            return person;
        person = new Person();
        person.setId(personDto.getId());
        person.setName(personDto.getName());
        person.setSurname(personDto.getSurname());
        person.setBirthday(personDto.getBirthday());
        return person;
    }

    public List<PersonDto> toDto(List<Person> persons) {
        return persons.stream().map(this::toDto).collect(Collectors.toList());
    }
}