package com.tsystems.javaschool.trains.mappers;

import com.tsystems.javaschool.trains.dto.TicketInfo;
import com.tsystems.javaschool.trains.model.Ticket;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TicketMapper {
    private final PersonMapper personMapper;
    private final TrainMapper trainMapper;

    public TicketMapper(PersonMapper personMapper, TrainMapper trainMapper) {
        this.personMapper = personMapper;
        this.trainMapper = trainMapper;
    }

    public TicketInfo toInfo(Ticket ticket) {
        TicketInfo ticketInfo = new TicketInfo();
        ticketInfo.setId(ticket.getId());
        ticketInfo.setPerson(personMapper.toDto(ticket.getPerson()));
        ticketInfo.setTrain(trainMapper.toDto(ticket.getTrain()));
        return ticketInfo;
    }

    public List<TicketInfo> toInfo(List<Ticket> tickets) {
        return tickets.stream().map(this::toInfo).collect(Collectors.toList());
    }

    // No ticket buying code here
}