package com.tsystems.javaschool.trains.mappers;

import com.tsystems.javaschool.trains.dao.TrainDao;
import com.tsystems.javaschool.trains.dto.TrainDto;
import com.tsystems.javaschool.trains.model.Train;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TrainMapper {
    private final TrainDao trainDao;

    public TrainMapper(TrainDao trainDao) {
        this.trainDao = trainDao;
    }

    public TrainDto toDto(Train train) {
        TrainDto trainDto = new TrainDto();
        trainDto.setId(train.getId());
        trainDto.setName(train.getName());
        trainDto.setNumberOfSeats(train.getNumberOfSeats());
        return trainDto;
    }

    public Train toTrain(TrainDto trainDto) {
        Train train = trainDao.get(trainDto.getId());
        if (train != null)
            return train;
        train = new Train();
        train.setId(trainDto.getId());
        train.setName(trainDto.getName());
        train.setNumberOfSeats(trainDto.getNumberOfSeats());
        return train;
    }

    public List<TrainDto> toDto(List<Train> trains) {
        return trains.stream().map(this::toDto).collect(Collectors.toList());
    }
}