package com.tsystems.javaschool.trains.mappers;

import com.tsystems.javaschool.trains.dto.SearchResultInfo;
import com.tsystems.javaschool.trains.model.SearchResult;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SearchResultMapper {
    private final TrainMapper trainMapper;
    private final TimetableMapper timetableMapper;

    public SearchResultMapper(TrainMapper trainMapper, TimetableMapper timetableMapper) {
        this.trainMapper = trainMapper;
        this.timetableMapper = timetableMapper;
    }

    public SearchResultInfo toInfo(SearchResult searchResult) {
        SearchResultInfo info = new SearchResultInfo();
        info.setTrain(trainMapper.toDto(searchResult.getTrain()));
        info.setArrival(timetableMapper.toInfo(searchResult.getArrival()));
        info.setDeparture(timetableMapper.toInfo(searchResult.getDeparture()));
        return info;
    }

    public List<SearchResultInfo> toInfo(List<SearchResult> searchResults) {
        return searchResults.stream().map(this::toInfo).collect(Collectors.toList());
    }
}
