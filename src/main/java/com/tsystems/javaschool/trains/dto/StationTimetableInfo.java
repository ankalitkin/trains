package com.tsystems.javaschool.trains.dto;

import lombok.Data;

import java.util.List;

@Data
public class StationTimetableInfo {
    private int id;
    private String name;
    private List<TimetableDto> timetable;
}
