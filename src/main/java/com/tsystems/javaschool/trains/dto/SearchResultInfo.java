package com.tsystems.javaschool.trains.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchResultInfo {
    private TrainDto train;
    private TimetableInfo departure;
    private TimetableInfo arrival;
}
