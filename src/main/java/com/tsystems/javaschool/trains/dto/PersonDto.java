package com.tsystems.javaschool.trains.dto;

import lombok.Data;

@Data
public class PersonDto {
    private int id;
    private String name;
    private String surname;
    private String birthday;
}
