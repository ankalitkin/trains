package com.tsystems.javaschool.trains.dto;

import lombok.Data;

import java.util.List;

@Data
public class PersonTicketsInfo {
    private int id;
    private String name;
    private String surname;
    private String birthday;
    private List<TicketInfo> tickets;
}
