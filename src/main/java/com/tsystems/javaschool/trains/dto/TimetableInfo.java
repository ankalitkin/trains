package com.tsystems.javaschool.trains.dto;

import lombok.Data;

@Data
public class TimetableInfo {
    private int id;
    private StationDto station;
    private int train_id;
    private String arrival;
    private String departure;
}
