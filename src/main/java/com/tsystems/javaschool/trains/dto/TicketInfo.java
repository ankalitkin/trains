package com.tsystems.javaschool.trains.dto;

import lombok.Data;

@Data
public class TicketInfo {
    private int id;
    private PersonDto person;
    private TrainDto train;
}
