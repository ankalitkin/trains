package com.tsystems.javaschool.trains.dto;

import lombok.Data;

@Data
public class TrainDto {
    private int id;
    private String name;
    private int numberOfSeats;
}
