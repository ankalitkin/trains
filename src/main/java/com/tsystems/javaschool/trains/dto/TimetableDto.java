package com.tsystems.javaschool.trains.dto;

import lombok.Data;

@Data
public class TimetableDto {
    private int id;
    private int station_id;
    private int train_id;
    private String arrival;
    private String departure;
}
