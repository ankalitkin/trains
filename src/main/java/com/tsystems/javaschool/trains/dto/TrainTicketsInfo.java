package com.tsystems.javaschool.trains.dto;

import lombok.Data;

import java.util.List;

@Data
public class TrainTicketsInfo {
    private int id;
    private String name;
    private int numberOfSeats;
    private List<TicketInfo> tickets;
}