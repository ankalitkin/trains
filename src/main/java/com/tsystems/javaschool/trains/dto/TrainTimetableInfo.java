package com.tsystems.javaschool.trains.dto;

import lombok.Data;

import java.util.List;

@Data
public class TrainTimetableInfo {
    private int id;
    private String name;
    private int numberOfSeats;
    private List<TimetableInfo> timetable;
}