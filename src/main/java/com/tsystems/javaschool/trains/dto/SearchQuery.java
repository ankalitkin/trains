package com.tsystems.javaschool.trains.dto;

import lombok.Data;

@Data
public class SearchQuery {
    private int fromStationId;
    private int toStationId;
    private String minTime;
    private String maxTime;
}
