package com.tsystems.javaschool.trains.dto;

import lombok.Data;

@Data
public class StationDto {
    private int id;
    private String name;
}
