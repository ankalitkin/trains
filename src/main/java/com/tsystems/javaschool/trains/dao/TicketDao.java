package com.tsystems.javaschool.trains.dao;

import com.tsystems.javaschool.trains.model.Ticket;

import java.util.List;

public interface TicketDao {
    int add(Ticket ticket);

    void addRange(Ticket[] ticket);

    void update(Ticket ticket);

    Ticket get(int id);

    List<Ticket> getAll();

    void delete(Ticket ticket);
}