package com.tsystems.javaschool.trains.dao;

import com.tsystems.javaschool.trains.model.Person;

import java.util.List;

public interface PersonDao {
    int add(Person person);

    void addRange(Person[] persons);

    void update(Person person);

    Person get(int id);

    Person find(String name, String surname, String birthday);

    List<Person> getAll();

    void delete(Person person);
}
