package com.tsystems.javaschool.trains.dao;

import com.tsystems.javaschool.trains.model.Train;
import com.tsystems.javaschool.trains.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class TrainDaoImpl implements TrainDao {

    private final SessionFactory sessionFactory;

    public TrainDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int add(Train train) {
        return (int) sessionFactory.getCurrentSession().save(train);
    }

    @Override
    public void addRange(Train[] trains) {
        Session currentSession = sessionFactory.getCurrentSession();
        for (Train train : trains) {
            currentSession.save(train);
        }
    }

    @Override
    public void update(Train train) {
        sessionFactory.getCurrentSession().update(train);
    }

    @Override
    public Train get(int id) {
        return sessionFactory.getCurrentSession().get(Train.class, id);
    }

    @Override
    public List<Train> getAll() {
        return HibernateUtil.loadAllData(Train.class, sessionFactory.getCurrentSession());
    }

    @Override
    public void delete(Train train) {
        sessionFactory.getCurrentSession().delete(train);
    }
}
