package com.tsystems.javaschool.trains.dao;

import com.tsystems.javaschool.trains.model.Ticket;
import com.tsystems.javaschool.trains.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class TicketDaoImpl implements TicketDao {

    private final SessionFactory sessionFactory;

    public TicketDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int add(Ticket ticket) {
        return (int) sessionFactory.getCurrentSession().save(ticket);
    }

    @Override
    public void addRange(Ticket[] tickets) {
        Session currentSession = sessionFactory.getCurrentSession();
        for (Ticket ticket : tickets) {
            currentSession.save(ticket);
        }
    }

    @Override
    public void update(Ticket ticket) {
        sessionFactory.getCurrentSession().update(ticket);
    }

    @Override
    public Ticket get(int id) {
        return sessionFactory.getCurrentSession().get(Ticket.class, id);
    }

    @Override
    public List<Ticket> getAll() {
        return HibernateUtil.loadAllData(Ticket.class, sessionFactory.getCurrentSession());
    }

    @Override
    public void delete(Ticket ticket) {
        sessionFactory.getCurrentSession().delete(ticket);
    }
}
