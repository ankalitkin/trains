package com.tsystems.javaschool.trains.dao;

import com.tsystems.javaschool.trains.model.Person;
import com.tsystems.javaschool.trains.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@Transactional
public class PersonDaoImpl implements PersonDao {

    private final SessionFactory sessionFactory;

    public PersonDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int add(Person person) {
        return (int) sessionFactory.getCurrentSession().save(person);
    }

    @Override
    public void addRange(Person[] persons) {
        Session currentSession = sessionFactory.getCurrentSession();
        for (Person person : persons) {
            currentSession.save(person);
        }
    }

    @Override
    public void update(Person person) {
        sessionFactory.getCurrentSession().update(person);
    }

    @Override
    public Person get(int id) {
        return sessionFactory.getCurrentSession().get(Person.class, id);
    }

    @Override
    public Person find(String name, String surname, String birthday) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Person> cr = cb.createQuery(Person.class);
        Root<Person> root = cr.from(Person.class);
        Predicate[] predicates = new Predicate[]{
                cb.equal(root.get("name"), name),
                cb.equal(root.get("surname"), surname),
                cb.equal(root.get("birthday"), birthday),
        };
        cr.select(root).where(predicates);
        return session.createQuery(cr).uniqueResult();
    }

    @Override
    public List<Person> getAll() {
        return HibernateUtil.loadAllData(Person.class, sessionFactory.getCurrentSession());
    }

    @Override
    public void delete(Person person) {
        sessionFactory.getCurrentSession().delete(person);
    }
}
