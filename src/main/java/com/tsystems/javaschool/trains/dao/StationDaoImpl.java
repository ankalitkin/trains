package com.tsystems.javaschool.trains.dao;

import com.tsystems.javaschool.trains.model.Station;
import com.tsystems.javaschool.trains.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class StationDaoImpl implements StationDao {

    private final SessionFactory sessionFactory;

    public StationDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int add(Station station) {
        return (int) sessionFactory.getCurrentSession().save(station);
    }

    @Override
    public void addRange(Station[] stations) {
        Session currentSession = sessionFactory.getCurrentSession();
        for (Station station : stations) {
            currentSession.save(station);
        }
    }

    @Override
    public void update(Station station) {
        sessionFactory.getCurrentSession().update(station);
    }

    @Override
    public Station get(int id) {
        return sessionFactory.getCurrentSession().get(Station.class, id);
    }

    @Override
    public List<Station> getAll() {
        return HibernateUtil.loadAllData(Station.class, sessionFactory.getCurrentSession());
    }

    @Override
    public void delete(Station station) {
        sessionFactory.getCurrentSession().delete(station);
    }
}
