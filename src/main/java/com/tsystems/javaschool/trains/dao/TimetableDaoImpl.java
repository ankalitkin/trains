package com.tsystems.javaschool.trains.dao;

import com.tsystems.javaschool.trains.model.Timetable;
import com.tsystems.javaschool.trains.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class TimetableDaoImpl implements TimetableDao {

    private final SessionFactory sessionFactory;

    public TimetableDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int add(Timetable timetable) {
        return (int) sessionFactory.getCurrentSession().save(timetable);
    }

    @Override
    public void addRange(Timetable[] timetable) {
        Session currentSession = sessionFactory.getCurrentSession();
        for (Timetable entry : timetable) {
            currentSession.save(entry);
        }
    }

    @Override
    public void update(Timetable timetable) {
        sessionFactory.getCurrentSession().update(timetable);
    }

    @Override
    public Timetable get(int id) {
        return sessionFactory.getCurrentSession().get(Timetable.class, id);
    }

    @Override
    public List<Timetable> getAll() {
        return HibernateUtil.loadAllData(Timetable.class, sessionFactory.getCurrentSession());
    }

    @Override
    public void delete(Timetable timetable) {
        sessionFactory.getCurrentSession().delete(timetable);
    }
}
