package com.tsystems.javaschool.trains.dao;

import com.tsystems.javaschool.trains.model.Station;

import java.util.List;

public interface StationDao {
    int add(Station station);

    void addRange(Station[] stations);

    void update(Station station);

    Station get(int id);

    List<Station> getAll();

    void delete(Station station);
}