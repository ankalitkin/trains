package com.tsystems.javaschool.trains.dao;

import com.tsystems.javaschool.trains.model.Train;

import java.util.List;

public interface TrainDao {
    int add(Train train);

    void addRange(Train[] trains);

    void update(Train train);

    Train get(int id);

    List<Train> getAll();

    void delete(Train train);
}