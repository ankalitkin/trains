package com.tsystems.javaschool.trains.dao;

import com.tsystems.javaschool.trains.model.Timetable;

import java.util.List;

public interface TimetableDao {
    int add(Timetable timetable);

    void addRange(Timetable[] timetable);

    void update(Timetable timetable);

    Timetable get(int id);

    List<Timetable> getAll();

    void delete(Timetable timetable);
}
