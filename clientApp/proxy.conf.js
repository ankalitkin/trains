const PROXY_CONFIG = {
  "/api": {
    "target": "http://localhost:8080",
    "secure": false,
    "logLevel": "debug",
    "pathRewrite": {
      "^/api": "/Trains_war_exploded"
    }
  }
};

module.exports = PROXY_CONFIG;
