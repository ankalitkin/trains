import {Train} from "./train";
import {TimetableInfo} from "./timetableInfo";

export class SearchResultInfo {
  public train: Train;
  public departure: TimetableInfo;
  public arrival: TimetableInfo;
}
