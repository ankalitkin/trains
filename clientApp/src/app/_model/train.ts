export class Train {
  public id: number;
  public name: String;
  public numberOfSeats: number;
}
