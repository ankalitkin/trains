export class SearchQuery {
  public fromStationId: number;
  public toStationId: number;
  public minTime: String;
  public maxTime: String;
}
