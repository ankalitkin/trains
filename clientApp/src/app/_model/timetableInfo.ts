import {Station} from "./station";

export class TimetableInfo {
  public id: number;
  public station: Station;
  public train_id: number;
  public arrival: String;
  public departure: String;
}
