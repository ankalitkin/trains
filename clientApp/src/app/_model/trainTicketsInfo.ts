import {TicketInfo} from "./ticketInfo";

export class TrainTicketsInfo {
  public id: number;
  public name: String;
  public numberOfSeats: number;
  public tickets: TicketInfo[];
}
