export class Person {
  public id: number;
  public name: String;
  public surname: String;
  public birthday: String;
}
