import {TicketInfo} from "./ticketInfo";

export class PersonTicketsInfo {
  public id: number;
  public name: String;
  public surname: String;
  public birthday: String;
  public tickets: TicketInfo[];
}
