export class Timetable {
  public id: number;
  public station_id: number;
  public train_id: number;
  public arrival: String;
  public departure: String;
}
