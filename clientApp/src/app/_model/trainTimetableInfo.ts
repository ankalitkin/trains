import {TimetableInfo} from "./timetableInfo";

export class TrainTimetableInfo {
  public id: number;
  public name: String;
  public numberOfSeats: number;
  public timetable: TimetableInfo[];
}
