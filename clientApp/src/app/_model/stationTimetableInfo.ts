import {Timetable} from "./timetable";

export class StationTimetableInfo {
  public id: number;
  public name: String;
  public timetable: Timetable[];
}
