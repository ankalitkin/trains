import {Person} from "./person";
import {Train} from "./train";

export class TicketInfo {
  public id: number;
  public person: Person;
  public train: Train;
}
