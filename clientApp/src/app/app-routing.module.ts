import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PersonTable} from "./person/person-table";


const routes: Routes = [
  {path: 'person', component: PersonTable}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
