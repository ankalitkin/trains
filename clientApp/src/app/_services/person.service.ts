import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Person} from "../_model/person";

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  constructor(private httpClient: HttpClient) {
  }

  getPersons(): Observable<Person[]> {
    return this.httpClient.get<Person[]>('api/person/');
  }

  getPersonByKey(key: string): Observable<Person> {
    return this.httpClient.get<Person>(`api/person/${key}`);
  }

  updatePerson(key: string, Person: Person): Observable<void> {
    return this.httpClient.put<void>(`api/person/${key}`, Person);
  }

  addPerson(Person: Person): Observable<void> {
    return this.httpClient.post<void>(`api/person/`, Person);
  }

  deletePerson(key: string): Observable<void> {
    return this.httpClient.delete<void>(`api/person/${key}`);
  }
}

