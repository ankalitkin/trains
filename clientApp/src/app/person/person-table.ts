import {Component, OnInit} from '@angular/core';
import {PersonService} from "../_services/person.service";
import {Observable} from "rxjs";
import {Person} from "../_model/person";

@Component({
  selector: 'person-table',
  templateUrl: './person-table.component.html',
  styleUrls: ['./person-table.component.scss']
})
export class PersonTable implements OnInit {
  private persons: Observable<Person[]>;

  constructor(private personService: PersonService) {
    this.persons = personService.getPersons();
  }

  ngOnInit() {
  }

}
